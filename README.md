
# 1. Micro Stack
___

- [1. Micro Stack](#1-micro-stack)
  - [1.1. Controller](#11-controller)
    - [1.1.1. <u>Installing Micro Stack</u>](#111-uinstalling-micro-stacku)
    - [1.1.2. <u>Initialise</u>](#112-uinitialiseu)
    - [1.1.3. <u>Get Admin passwd</u>](#113-uget-admin-passwdu)
    - [1.1.4. <u>Configure Profile</u>](#114-uconfigure-profileu)
    - [1.1.5. <u>Set Environment</u>](#115-uset-environmentu)
    - [1.1.6. <u>OpenStack Clients</u>](#116-uopenstack-clientsu)
    - [1.1.7. <u>Clear Network</u>](#117-uclear-networku)
    - [1.1.8. <u>Set net Networks</u>](#118-uset-net-networksu)
    - [1.1.9. <u>Bridge Workaround</u>](#119-ubridge-workaroundu)
    - [1.1.10. <u>Launch Instance</u>](#1110-ulaunch-instanceu)
    - [1.1.11. <u>Make Workaournd Permanent</u>](#1111-umake-workaournd-permanentu)
    - [1.1.12. <u>Use it</u>](#1112-uuse-itu)
  - [1.2. Computes](#12-computes)
    - [1.2.1. <u>Configure</u>](#121-uconfigureu)


## 1.1. Controller
___

### 1.1.1. <u>Installing Micro Stack</u>

```bash
sudo snap install microstack --devmode --beta
```


### 1.1.2. <u>Initialise</u>

```bash
<<<<<<< HEAD
sudo microstack init --control --default-source-ip 172.16.0.10
# yes
# 172.16.0.10
=======
sudo microstack init --control --default-source-ip 192.168.0.150
# yes
# 192.168.0.150
>>>>>>> 7459746b1e05730eac5c5f89220e2c984ba5f8c9
# no 
# no
```

### 1.1.3. <u>Get Admin passwd</u>

```bash
sudo snap get microstack config.credentials.keystone-password
# https localhost
```


### 1.1.4. <u>Configure Profile</u>
```bash
echo "export OS_AUTH_URL=https://172.16.0.10:5000/v3/
export OS_PROJECT_NAME="admin"
export OS_USER_DOMAIN_NAME="Default"
if [ -z "$OS_USER_DOMAIN_NAME" ]; then unset OS_USER_DOMAIN_NAME; fi
export OS_PROJECT_DOMAIN_ID="default"
if [ -z "$OS_PROJECT_DOMAIN_ID" ]; then unset OS_PROJECT_DOMAIN_ID; fi
unset OS_TENANT_ID
unset OS_TENANT_NAME
export OS_USERNAME="admin"
export OS_PASSWORD=$(sudo snap get microstack config.credentials.keystone-password)
export OS_REGION_NAME="microstack"
if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi
export OS_INTERFACE=public
export OS_IDENTITY_API_VERSION=3" >> ~/admin.rc
```

### 1.1.5. <u>Set Environment</u>

```bash
mkdir ~/certs
cp /var/snap/microstack/common/etc/ssl/certs/cacert.pem certs/
cp /var/snap/microstack/common/etc/ssl/certs/cert.pem certs
sudo cp /var/snap/microstack/common/etc/ssl/private/key.pem certs/
sudo chown vagrant certs/key.pem

echo "export OS_CACERT=~/certs/cacert.pem
export OS_CERT=~/certs/cert.pem
export OS_KEY=~/certs/key.pem" >> ~/admin.rc
```

### 1.1.6. <u>OpenStack Clients</u>

```bash
# - Download Source from Horizon
sudo snap install openstackclients
sudo snap alias microstack.ovs-vsctl ovs-vsctl
```

### 1.1.7. <u>Clear Network</u>

```bash
# Source
source admin.rc

# Delete Ports
for i in `openstack port list | grep ACTIVE | awk  '{print $2}'`; do openstack router remove port test-router $i; done

# Delete Router
openstack router delete test-router

# Delete networks
openstack network delete test
openstack network delete external
```

### 1.1.8. <u>Set net Networks</u>

```bash
# External Network
openstack network create  --share --external \
  --provider-physical-network physnet1 \
  --provider-network-type flat external

openstack subnet create --network external \
  --allocation-pool start=172.16.1.180,end=172.16.1.250 \
  --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 external-sub
 
# Internal Network
openstack network create test
openstack subnet create --network test --subnet-range 10.10.10.0/24 test

# Router
openstack router create router-test --project admin --enable
openstack router set --enable --enable-snat --external-gateway external router-test
openstack router add subnet router-test test
```


### 1.1.9. <u>Bridge Workaround</u>

```bash
# Thanks to: https://connection.rnascimento.com/2021/03/08/openstack-single-node-microstack/
sudo ovs-vsctl add-port br-ex eth2
```

### 1.1.10. <u>Launch Instance</u>

```bash
microstack launch cirros --name test
```

### 1.1.11. <u>Make Workaournd Permanent</u>

```bash
sudo -i

cat << EOF > /usr/local/bin/microstack-br-workaround
#!/bin/bash
# Workaround to enable physical network access to MicroStack
# Adds the server physical ip address to br-ex.

ip addr flush dev eth2
ip addr add 172.16.1.10/24 dev br-ex
ip link set br-ex up 
EOF
chmod +x /usr/local/bin/microstack-br-workaround


cat << EOF > /etc/systemd/system/microstack-br-workaround.service
[Unit]
Description=Service for adding physical ip to microstack bridge
Requires=snap.microstack.external-bridge.service
After=snap.microstack.external-bridge.service

[Service]
ExecStart=/usr/local/bin/microstack-br-workaround
SyslogIdentifier=microstack-br-workaround
Restart=no
WorkingDirectory=/usr/local/bin
TimeoutStopSec=30
Type=oneshot

[Install]
WantedBy=multi-user.target
EOF


# Enable
systemctl daemon-reload
systemctl enable microstack-br-workaround.service
```

### 1.1.12. <u>Use it</u>

```bash
exit
source admin.rc
nova list
# Add New Cnomputes
sudo microstack add-compute
```

## 1.2. Computes

### 1.2.1. <u>Configure</u>

```bash
sudo snap install microstack --devmode --beta

# Compute1
sudo microstack init --compute --default-source-ip 192.168.0.121
# Compute2
sudo microstack init --compute --default-source-ip 192.168.0.122

```